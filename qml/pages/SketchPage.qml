import QtQuick 2.0
import Sailfish.Silica 1.0
import "../items"

Page {
    id: sketchPage

    SketchArea {
        id: area
        anchors.fill: parent
    }
}
