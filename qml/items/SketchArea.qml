import QtQuick 2.0

SketchView {
    id: area
    autoRepaint: false

    property color fingerLineColor: Qt.rgba(1.0, 0.0, 0.0, 1.0)
    property color stylusLineColor: Qt.rgba(0.0, 0.0, 0.0, 1.0)
    property color eraserLineColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property real curLineWidth: 4

    function _boundingBox(points) {
        if (!points.length) return Qt.rect(0, 0, 0, 0);
        var x1 = points[0].x, y1 = points[0].y;
        var x2 = x1, y2 = y1;

        for (var i = 1; i < points.length; i++) {
            if (points[i].x < x1) x1 = points[i].x;
            if (points[i].y < y1) y1 = points[i].y;
            if (points[i].x > x2) x2 = points[i].x;
            if (points[i].y > y2) y2 = points[i].y;
        }

        return Qt.rect(x1, y1, x2 - x1, y2 - y1);
    }

    function _isStylusOn() {
        return stylus.pressure > 0.15;
    }
    function _isEraserOn() {
        return !_isStylusOn() && stylus.y > 0.85;
    }

    MouseArea {
        anchors.fill: parent
        preventStealing: true

        onPressed: {
            var point = Qt.point(mouse.x, mouse.y);
            var stroke = {
                lineWidth : curLineWidth,
                lineColor : fingerLineColor,
                stylus : false,
                eraser : false,
                boundingBox : Qt.rect(0, 0, area.width, area.height),
                points : [point]
            }
            strokes.push(stroke);
            canvas.requestPaint();
        }
        onReleased: {
            canvas.requestPaint();
        }
        onPositionChanged: {
            var cur_stroke = strokes[area.strokes.length - 1];
            var point = Qt.point(mouse.x, mouse.y);
            var prev_point = cur_stroke.points[cur_stroke.points.length - 1];
            cur_stroke.points.push(point);
            cur_stroke.boundingBox = _boundingBox(cur_stroke.points);

            var rect = Qt.rect(Math.floor(Math.min(point.x, prev_point.x) - cur_stroke.lineWidth/2),
                               Math.floor(Math.min(point.y, prev_point.y) - cur_stroke.lineWidth/2),
                               Math.ceil(Math.abs(point.x - prev_point.x) + cur_stroke.lineWidth),
                               Math.ceil(Math.abs(point.y - prev_point.y) + cur_stroke.lineWidth));

            if (!cur_stroke.stylus) {
                if (_isStylusOn()) {
                    cur_stroke.stylus = true;
                    cur_stroke.lineColor = stylusLineColor;
                    canvas.requestPaint();
                    return;
                } else if (_isEraserOn()) {
                    cur_stroke.stylus = true;
                    cur_stroke.eraser = true;
                    cur_stroke.lineColor = eraserLineColor;
                    canvas.requestPaint();
                    return;
                }
            }

            canvas.markDirty(rect);
        }
    }
}
