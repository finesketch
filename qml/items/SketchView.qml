import QtQuick 2.0

Item {
    id: view

    property alias fillColor: rectangle.color
    property alias border: rectangle.border

    property var strokes: []

    property bool autoRepaint: true
    property alias canvas: canvas

    function _drawStroke(ctx, stroke) {
        var points = stroke.points;
        ctx.save();
        ctx.lineWidth = stroke.lineWidth;
        ctx.strokeStyle = stroke.lineColor;
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';
        ctx.beginPath();
        ctx.moveTo(points[0].x, points[0].y);
        for (var i = 1; i < points.length; i++) {
            ctx.lineTo(points[i].x, points[i].y);
        }
        ctx.stroke();
        ctx.restore();
    }

    function _intersect(r1, r2) {
        if (!r1.width || !r2.width) return false;
        if (!r1.height || !r2.height) return false;

        if (r1.x >= r2.x + r2.width) return false;
        if (r1.y >= r2.y + r2.height) return false;
        if (r2.x >= r1.x + r1.width) return false;
        if (r2.y >= r1.y + r1.height) return false;
        return true;
    }

    Rectangle {
        id: rectangle
        anchors.fill: parent

        color: Qt.rgba(1.0, 1.0, 1.0, 0.3)
        radius: 10.0
    }

    Canvas {
        id: canvas
        anchors.fill: parent

        renderTarget: Canvas.Image
        renderStrategy: Canvas.Immediate

        onPaint: {
            var ctx = canvas.getContext('2d');
            ctx.clearRect(region.x, region.y, region.width, region.height);

            ctx.save();
            ctx.beginPath();
            ctx.rect(region.x, region.y, region.width, region.height);
            ctx.clip();

            for (var i = 0; i < strokes.length; i++) {
                if (_intersect(region, strokes[i].boundingBox)) {
                    _drawStroke(ctx, strokes[i]);
                }
            }

            ctx.restore();
        }
    }

    onStrokesChanged: {
        if (autoRepaint) {
            canvas.requestPaint();
        }
    }
}
