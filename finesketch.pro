# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = finesketch

CONFIG += sailfishapp

LIBS += -lgato

SOURCES += src/finesketch.cpp \
    src/stylus.cpp

OTHER_FILES += qml/finesketch.qml \
    qml/cover/CoverPage.qml \
    rpm/finesketch.changes.in \
    rpm/finesketch.spec \
    rpm/finesketch.yaml \
    translations/*.ts \
    finesketch.desktop \
    finesketch.png \
    qml/items/SketchView.qml \
    qml/pages/SketchPage.qml \
    qml/items/SketchArea.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/finesketch-de.ts

HEADERS += \
    src/stylus.h

