#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtQml/QQmlContext>
#include <sailfishapp.h>
#include "stylus.h"

int main(int argc, char *argv[])
{
	QGuiApplication *app = SailfishApp::application(argc, argv);
	QQuickView *view = SailfishApp::createView();

	Stylus *stylus = new Stylus();
	stylus->connectDevice("F4:6A:BC:10:4F:E9");

	view->rootContext()->setContextProperty("stylus", stylus);
	view->setSource(SailfishApp::pathTo("qml/finesketch.qml"));

	view->show();

	int res = app->exec();

	delete stylus;

	return res;
}

