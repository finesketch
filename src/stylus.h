#ifndef STYLUS_H
#define STYLUS_H

#include <QObject>
#include <gato/gatocentralmanager.h>
#include <gato/gatoperipheral.h>

class Stylus : public QObject
{
	Q_OBJECT
	Q_PROPERTY(qreal pressure READ pressure NOTIFY pressureChanged)
	Q_PROPERTY(qreal x READ x NOTIFY xChanged)
	Q_PROPERTY(qreal y READ y NOTIFY yChanged)
	Q_PROPERTY(qreal z READ z NOTIFY zChanged)

public:
	explicit Stylus(QObject *parent = 0);
	~Stylus();

	qreal pressure() const;
	qreal x() const;
	qreal y() const;
	qreal z() const;

signals:
	void autoUpdateChanged();
	void pressureChanged();
	void xChanged();
	void yChanged();
	void zChanged();

public slots:
	void connectToAnyDevice();
	void connectDevice(const QString &addr);
	void disconnectDevice();

private:
	void connectToPeripheral(GatoPeripheral *peripheral);

private slots:
	void handleDiscoveredPeripheral(GatoPeripheral *peripheral, int rssi);
	void handleConnected();
	void handleDisconnected();
	void handleServices();
	void handleCharacteristics(const GatoService &service);
	void handleValueUpdated(const GatoCharacteristic &characteristic, const QByteArray &value);
	void handleReport(int p, int x, int y, int z);

private:
	GatoCentralManager *_manager;
	GatoPeripheral *_peripheral;

	qreal _p;
	qreal _x;
	qreal _y;
	qreal _z;
};

#endif // STYLUS_H
